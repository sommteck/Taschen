namespace Taschenrechner_GUI
{
    partial class taschenrechner
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.txt_input = new System.Windows.Forms.TextBox();
            this.cmd_addieren = new System.Windows.Forms.Button();
            this.cmd_end = new System.Windows.Forms.Button();
            this.cmd_clear = new System.Windows.Forms.Button();
            this.cmd_calculate = new System.Windows.Forms.Button();
            this.cmd_modulo = new System.Windows.Forms.Button();
            this.cmd_prozent = new System.Windows.Forms.Button();
            this.cmd_wurzel = new System.Windows.Forms.Button();
            this.cmd_quadrieren = new System.Windows.Forms.Button();
            this.cmd_dividieren = new System.Windows.Forms.Button();
            this.cmd_multiplizieren = new System.Windows.Forms.Button();
            this.cmd_subtrahieren = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txt_input
            // 
            this.txt_input.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_input.Location = new System.Drawing.Point(58, 43);
            this.txt_input.Name = "txt_input";
            this.txt_input.Size = new System.Drawing.Size(316, 35);
            this.txt_input.TabIndex = 0;
            this.txt_input.TabStop = false;
            // 
            // cmd_addieren
            // 
            this.cmd_addieren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_addieren.Location = new System.Drawing.Point(58, 120);
            this.cmd_addieren.Name = "cmd_addieren";
            this.cmd_addieren.Size = new System.Drawing.Size(47, 35);
            this.cmd_addieren.TabIndex = 1;
            this.cmd_addieren.TabStop = false;
            this.cmd_addieren.Text = "+";
            this.cmd_addieren.UseVisualStyleBackColor = true;
            this.cmd_addieren.Click += new System.EventHandler(this.cmd_addieren_Click);
            // 
            // cmd_end
            // 
            this.cmd_end.Location = new System.Drawing.Point(270, 323);
            this.cmd_end.Name = "cmd_end";
            this.cmd_end.Size = new System.Drawing.Size(75, 44);
            this.cmd_end.TabIndex = 2;
            this.cmd_end.TabStop = false;
            this.cmd_end.Text = "&Beenden";
            this.cmd_end.UseVisualStyleBackColor = true;
            this.cmd_end.Click += new System.EventHandler(this.cmd_end_Click);
            // 
            // cmd_clear
            // 
            this.cmd_clear.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_clear.Location = new System.Drawing.Point(270, 183);
            this.cmd_clear.Name = "cmd_clear";
            this.cmd_clear.Size = new System.Drawing.Size(75, 35);
            this.cmd_clear.TabIndex = 3;
            this.cmd_clear.TabStop = false;
            this.cmd_clear.Text = "C";
            this.cmd_clear.UseVisualStyleBackColor = true;
            this.cmd_clear.Click += new System.EventHandler(this.cmd_clear_Click);
            // 
            // cmd_calculate
            // 
            this.cmd_calculate.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_calculate.Location = new System.Drawing.Point(270, 120);
            this.cmd_calculate.Name = "cmd_calculate";
            this.cmd_calculate.Size = new System.Drawing.Size(75, 35);
            this.cmd_calculate.TabIndex = 4;
            this.cmd_calculate.TabStop = false;
            this.cmd_calculate.Text = "=";
            this.cmd_calculate.UseVisualStyleBackColor = true;
            this.cmd_calculate.Click += new System.EventHandler(this.cmd_calculate_Click);
            // 
            // cmd_modulo
            // 
            this.cmd_modulo.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_modulo.Location = new System.Drawing.Point(154, 330);
            this.cmd_modulo.Name = "cmd_modulo";
            this.cmd_modulo.Size = new System.Drawing.Size(56, 37);
            this.cmd_modulo.TabIndex = 5;
            this.cmd_modulo.TabStop = false;
            this.cmd_modulo.Text = "mod";
            this.cmd_modulo.UseVisualStyleBackColor = true;
            this.cmd_modulo.Click += new System.EventHandler(this.cmd_modulo_Click);
            // 
            // cmd_prozent
            // 
            this.cmd_prozent.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_prozent.Location = new System.Drawing.Point(58, 330);
            this.cmd_prozent.Name = "cmd_prozent";
            this.cmd_prozent.Size = new System.Drawing.Size(47, 37);
            this.cmd_prozent.TabIndex = 6;
            this.cmd_prozent.TabStop = false;
            this.cmd_prozent.Text = "%";
            this.cmd_prozent.UseVisualStyleBackColor = true;
            this.cmd_prozent.Click += new System.EventHandler(this.cmd_prozent_Click);
            // 
            // cmd_wurzel
            // 
            this.cmd_wurzel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_wurzel.Location = new System.Drawing.Point(154, 260);
            this.cmd_wurzel.Name = "cmd_wurzel";
            this.cmd_wurzel.Size = new System.Drawing.Size(56, 33);
            this.cmd_wurzel.TabIndex = 7;
            this.cmd_wurzel.TabStop = false;
            this.cmd_wurzel.Text = "√";
            this.cmd_wurzel.UseVisualStyleBackColor = true;
            this.cmd_wurzel.Click += new System.EventHandler(this.cmd_wurzel_Click);
            // 
            // cmd_quadrieren
            // 
            this.cmd_quadrieren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_quadrieren.Location = new System.Drawing.Point(58, 260);
            this.cmd_quadrieren.Name = "cmd_quadrieren";
            this.cmd_quadrieren.Size = new System.Drawing.Size(47, 33);
            this.cmd_quadrieren.TabIndex = 8;
            this.cmd_quadrieren.TabStop = false;
            this.cmd_quadrieren.Text = "x²";
            this.cmd_quadrieren.UseVisualStyleBackColor = true;
            this.cmd_quadrieren.Click += new System.EventHandler(this.cmd_quadrieren_Click);
            // 
            // cmd_dividieren
            // 
            this.cmd_dividieren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_dividieren.Location = new System.Drawing.Point(154, 183);
            this.cmd_dividieren.Name = "cmd_dividieren";
            this.cmd_dividieren.Size = new System.Drawing.Size(56, 35);
            this.cmd_dividieren.TabIndex = 9;
            this.cmd_dividieren.TabStop = false;
            this.cmd_dividieren.Text = "/";
            this.cmd_dividieren.UseVisualStyleBackColor = true;
            this.cmd_dividieren.Click += new System.EventHandler(this.cmd_dividieren_Click);
            // 
            // cmd_multiplizieren
            // 
            this.cmd_multiplizieren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_multiplizieren.Location = new System.Drawing.Point(154, 120);
            this.cmd_multiplizieren.Name = "cmd_multiplizieren";
            this.cmd_multiplizieren.Size = new System.Drawing.Size(56, 35);
            this.cmd_multiplizieren.TabIndex = 10;
            this.cmd_multiplizieren.TabStop = false;
            this.cmd_multiplizieren.Text = "*";
            this.cmd_multiplizieren.UseVisualStyleBackColor = true;
            this.cmd_multiplizieren.Click += new System.EventHandler(this.cmd_multiplizieren_Click);
            // 
            // cmd_subtrahieren
            // 
            this.cmd_subtrahieren.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_subtrahieren.Location = new System.Drawing.Point(58, 183);
            this.cmd_subtrahieren.Name = "cmd_subtrahieren";
            this.cmd_subtrahieren.Size = new System.Drawing.Size(47, 35);
            this.cmd_subtrahieren.TabIndex = 11;
            this.cmd_subtrahieren.TabStop = false;
            this.cmd_subtrahieren.Text = "-";
            this.cmd_subtrahieren.UseVisualStyleBackColor = true;
            this.cmd_subtrahieren.Click += new System.EventHandler(this.cmd_subtrahieren_Click);
            // 
            // taschenrechner
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 430);
            this.Controls.Add(this.cmd_subtrahieren);
            this.Controls.Add(this.cmd_multiplizieren);
            this.Controls.Add(this.cmd_dividieren);
            this.Controls.Add(this.cmd_quadrieren);
            this.Controls.Add(this.cmd_wurzel);
            this.Controls.Add(this.cmd_prozent);
            this.Controls.Add(this.cmd_modulo);
            this.Controls.Add(this.cmd_calculate);
            this.Controls.Add(this.cmd_clear);
            this.Controls.Add(this.cmd_end);
            this.Controls.Add(this.cmd_addieren);
            this.Controls.Add(this.txt_input);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "taschenrechner";
            this.Text = "Taschenrechner";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txt_input;
        private System.Windows.Forms.Button cmd_addieren;
        private System.Windows.Forms.Button cmd_end;
        private System.Windows.Forms.Button cmd_clear;
        private System.Windows.Forms.Button cmd_calculate;
        private System.Windows.Forms.Button cmd_modulo;
        private System.Windows.Forms.Button cmd_prozent;
        private System.Windows.Forms.Button cmd_wurzel;
        private System.Windows.Forms.Button cmd_quadrieren;
        private System.Windows.Forms.Button cmd_dividieren;
        private System.Windows.Forms.Button cmd_multiplizieren;
        private System.Windows.Forms.Button cmd_subtrahieren;
    }
}

