using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Taschenrechner_GUI
{
    public partial class taschenrechner : Form
    {
        private char operation = ' ';
        private double zahl;

        public taschenrechner()
        {
            InitializeComponent();
            txt_input.Focus();
        }

        private void cmd_end_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Interne Funktion zur Festlegung der Rechenoperation
        private void Berechnen()
        {
            try
            {
                switch (operation)
                {
                    case '+': zahl += Convert.ToDouble(txt_input.Text);
                                break;
                    case '-': zahl -= Convert.ToDouble(txt_input.Text);
                                break;
                    case '*': zahl *= Convert.ToDouble(txt_input.Text);
                                break;
                    case '/': if (Convert.ToDouble(txt_input.Text) != 0)
                                    zahl /= Convert.ToDouble(txt_input.Text);
                                else
                                    MessageBox.Show("Division durch 0 nicht erlaubt!");
                                break;
                    case '%': if (Convert.ToDouble(txt_input.Text) != 0)
                                    zahl %= Convert.ToDouble(txt_input.Text);
                                else
                                    MessageBox.Show("Division durch 0 nicht erlaubt!");
                                break;
                    case 'P': zahl = zahl / 100 * Convert.ToDouble(txt_input.Text);
                                break;
                    case 'Q': zahl = Math.Pow(Convert.ToDouble(txt_input.Text), 2);
                                break;
                    case 'W': zahl = Math.Sqrt(Convert.ToDouble(txt_input.Text));
                                break;
                    default: zahl = Convert.ToDouble(txt_input.Text);
                                break;
                }
            }

            catch
            {
                MessageBox.Show("Bitte nur Ziffern eingeben!");
                txt_input.Clear();
                txt_input.Focus();
            }
            txt_input.Text = zahl.ToString();
        }

        private void cmd_addieren_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = '+';
            txt_input.Focus();
        }

        private void cmd_subtrahieren_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = '-';
            txt_input.Focus();
        }

        private void cmd_multiplizieren_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = '*';
            txt_input.Focus();
        }

        private void cmd_dividieren_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = '/';
            txt_input.Focus();
        }

        private void cmd_quadrieren_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = 'Q';
            txt_input.Focus();
        }

        private void cmd_wurzel_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = 'W';
            txt_input.Focus();
        }

        private void cmd_prozent_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = 'P';
            txt_input.Focus();
        }

        private void cmd_modulo_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = '%';
            txt_input.Focus();
        }

        private void cmd_clear_Click(object sender, EventArgs e)
        {
            zahl = 0;
            operation = ' ';
            txt_input.Clear();
            txt_input.Focus();
        }

        private void cmd_calculate_Click(object sender, EventArgs e)
        {
            Berechnen();
            operation = ' ';
            txt_input.Focus();
        }
    }
}